export default interface GameEvent {
    name: string;
    source: any;
    data: any;
}
