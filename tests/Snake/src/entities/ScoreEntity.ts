import GameEntity from "../../../../src/entities/GameEntity";
import Renderer from "../../../../src/graphics/Renderer";
import SnakeGame, {Event} from "../SnakeGame";

export default class ScoreEntity extends GameEntity {
    readonly name = 'Score';

    private score: number;
    private gameEnd: boolean;
    private gameEndMessage = [
        'Space snake now dead snake :(',
        '   Press Space to continue   '
    ];
    private gameEndDrawMessage: string[] = [];
    private messageDrawTimer = 0;
    private readonly drawInterval = 20;

    init(): void {
        this.score = 0;
        this.gameEnd = false;
        this.events.on(Event.FruitEaten, () => this.updateScore());
        this.events.on(Event.SnakeDied, () => this.gameEnd = true);
        this.events.on(Event.GameRestart, () => {
            this.gameEnd = false;
            this.score = 0;
            this.gameEndDrawMessage = [];
        });
    }

    updateScore(): void {
        this.score++
        if (this.score > 0 && this.score % 5 === 0) {
            this.events.fire(Event.LvlUp);
        }
    }

    update(elapsedTime: number): void {
        if (this.gameEnd) {
            this.messageDrawTimer += elapsedTime;
            if (this.messageDrawTimer > this.drawInterval) {
                this.messageDrawTimer = 0;
                for (let i = 0; i < this.gameEndMessage.length; i++) {
                    if (!this.gameEndDrawMessage[i]?.length) {
                        this.gameEndDrawMessage[i] = '';
                    }

                    if (this.gameEndMessage[i].length > this.gameEndDrawMessage[i].length) {
                        let char = this.gameEndMessage[i][this.gameEndDrawMessage[i].length];
                        this.gameEndDrawMessage[i] += char;
                        return;
                    }
                }
            }
        }
    }


    draw(renderer: Renderer): void {
        renderer.drawText('Score: ' + this.score.toString().padStart(3, '0'),
            Math.floor(SnakeGame.MarginLeft + 60),
            Math.floor(SnakeGame.BlockSize * SnakeGame.BlockCount + 50 + SnakeGame.MarginTop),
            'white', 'monospace', 30);

        if (this.gameEnd) {
            renderer.drawText(this.gameEndDrawMessage,
                Math.floor(SnakeGame.MarginLeft - 170),
                Math.floor(SnakeGame.MarginTop - 80),
                '#dc3d3d', 'monospace', 40);
        }
    }
}
