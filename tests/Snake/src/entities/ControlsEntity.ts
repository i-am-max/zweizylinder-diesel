import GameEntity from "../../../../src/entities/GameEntity";
import Renderer from "../../../../src/graphics/Renderer";
import InputManager from "../../../../src/inputs/InputManager";
import {MoveDir} from "./SnakeEntity";
import {Event} from "../SnakeGame";

export default class ControlsEntity extends GameEntity {
    readonly name = 'Controls';

    private gameEnd: boolean;

    constructor(private inputManager: InputManager) {
        super();
    }

    init(): void {
        this.events.on(Event.SnakeDied, () => this.gameEnd = true);
    }

    update(elapsedTime: number): void {
        const keyboard = this.inputManager.getKeyboard();

        if (this.gameEnd && keyboard.isKeyPressed(' ')) {
            this.events.fire(Event.GameRestart);
            this.gameEnd = false;
            return;
        }

        let nextDir: MoveDir = null;
        if (keyboard.isKeyPressed('ArrowRight')) {
            nextDir = MoveDir.Right;
        } else if (keyboard.isKeyPressed('ArrowLeft')) {
            nextDir = MoveDir.Left;
        } else if (keyboard.isKeyPressed('ArrowUp')) {
            nextDir = MoveDir.Up;
        } else if (keyboard.isKeyPressed('ArrowDown')) {
            nextDir = MoveDir.Down;
        }

        if (nextDir) {
            this.events.fire(Event.SnakeDirChanged, nextDir);
        }
    }

    draw(renderer: Renderer): void {
    }
}
